object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Soft Inventive Lab (RubberGUI - Test task #2)'
  ClientHeight = 606
  ClientWidth = 969
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 185
    Top = 0
    Width = 5
    Height = 606
    ResizeStyle = rsUpdate
    ExplicitLeft = 179
    ExplicitTop = 1
    ExplicitHeight = 594
  end
  object Splitter2: TSplitter
    Left = 779
    Top = 0
    Width = 5
    Height = 606
    Align = alRight
    ResizeStyle = rsUpdate
    ExplicitLeft = 721
    ExplicitTop = 8
  end
  object Panel1: TPanel
    Left = 190
    Top = 0
    Width = 589
    Height = 606
    Align = alClient
    TabOrder = 0
    OnCanResize = Panel1CanResize
    ExplicitWidth = 585
    ExplicitHeight = 594
    object Splitter4: TSplitter
      Left = 1
      Top = 475
      Width = 587
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ResizeStyle = rsUpdate
      ExplicitLeft = 4
      ExplicitTop = 456
      ExplicitWidth = 569
    end
    object Splitter3: TSplitter
      Left = 1
      Top = 161
      Width = 587
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ResizeStyle = rsUpdate
      ExplicitWidth = 305
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 587
      Height = 160
      Align = alTop
      Anchors = [akLeft, akRight, akBottom]
      TabOrder = 0
      OnCanResize = Panel2CanResize
      ExplicitWidth = 583
      object Splitter5: TSplitter
        Left = 289
        Top = 1
        Width = 5
        Height = 158
        ResizeStyle = rsUpdate
      end
      object ListView1: TListView
        Left = 1
        Top = 1
        Width = 288
        Height = 158
        Align = alLeft
        Columns = <>
        TabOrder = 0
      end
      object RichEdit1: TRichEdit
        Left = 294
        Top = 1
        Width = 292
        Height = 158
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'RichEdit1')
        ParentFont = False
        TabOrder = 1
        ExplicitWidth = 288
      end
    end
    object TreeView1: TTreeView
      Left = 1
      Top = 164
      Width = 587
      Height = 311
      Align = alClient
      Indent = 19
      TabOrder = 1
      ExplicitTop = 163
      ExplicitWidth = 583
      ExplicitHeight = 316
    end
    object Panel5: TPanel
      Left = 1
      Top = 480
      Width = 587
      Height = 125
      Align = alBottom
      Caption = 'Panel5'
      TabOrder = 2
      OnCanResize = Panel5CanResize
      object StringGrid1: TStringGrid
        Left = 1
        Top = 1
        Width = 585
        Height = 123
        Align = alClient
        TabOrder = 0
        ExplicitTop = 164
        ExplicitWidth = 587
        ExplicitHeight = 395
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 606
    Align = alLeft
    Caption = 'Panel3'
    TabOrder = 1
    OnCanResize = Panel3CanResize
    ExplicitLeft = 8
    ExplicitTop = -8
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 183
      Height = 604
      Align = alClient
      Lines.Strings = (
        'Memo1')
      TabOrder = 0
      ExplicitLeft = 34
      ExplicitTop = 128
      ExplicitWidth = 135
      ExplicitHeight = 89
    end
  end
  object Panel4: TPanel
    Left = 784
    Top = 0
    Width = 185
    Height = 606
    Align = alRight
    Caption = 'Panel4'
    TabOrder = 2
    OnCanResize = Panel4CanResize
    ExplicitLeft = 816
    ExplicitTop = 336
    ExplicitHeight = 41
    object Memo2: TMemo
      Left = 1
      Top = 1
      Width = 183
      Height = 604
      Align = alClient
      Lines.Strings = (
        'Memo2')
      TabOrder = 0
      ExplicitLeft = 72
      ExplicitTop = 232
      ExplicitWidth = 185
      ExplicitHeight = 89
    end
  end
end
