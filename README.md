Интерфейсное.
Реализовать форму с несколькими контейнерами и сплиттерами между ними, как показано на вложенном файле (sketch.png). При изменении размера окна все контейнеры должны менять размер пропорционально текущему, и сохранять свои размеры при перезапуске в конфигурационном файле.
![sketch.png](https://bitbucket.org/repo/EpxqRR/images/2182611694-sketch.png)