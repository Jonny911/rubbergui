unit RubberForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls, ButtonGroup, IniFiles, ComCtrls;


type
  TForm1 = class(TForm)
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel1: TPanel;
    Splitter4: TSplitter;
    Panel2: TPanel;
    Splitter3: TSplitter;
    TreeView1: TTreeView;
    ListView1: TListView;
    Splitter5: TSplitter;
    RichEdit1: TRichEdit;
    Panel3: TPanel;
    Memo1: TMemo;
    Panel4: TPanel;
    Memo2: TMemo;
    Panel5: TPanel;
    StringGrid1: TStringGrid;
    procedure Panel2CanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure Panel1CanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Panel3CanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure Panel4CanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure Panel5CanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
  end;

const
  IniFilename = 'gui.ini';

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure SaveControl(ctrl: TControl);
var
    Path: String;
    Ini: TINIFile;
begin
  Path:= ExtractFilePath (Application.ExeName);
  Ini := TINIFile.Create (Path + IniFilename);
  try
    with ctrl do
    begin
      Ini.WriteInteger(Name, 'Left',   Left);
      Ini.WriteInteger(Name, 'Top',    Top);
      Ini.WriteInteger(Name, 'Width',  Width);
      Ini.WriteInteger(Name, 'Height', Height);
    end;
  finally
    Ini.Free;
  end;
end;

procedure LoadControl (ctrl: TControl);
var
    Path: String;
    Ini: TINIFile;
begin
  Path:= ExtractFilePath (Application.ExeName);
  Ini := TINIFile.Create (Path + IniFilename);
  try
    with ctrl do
    begin
      Left   := Ini.ReadInteger (Name, 'Left',   Left);
      Top    := Ini.ReadInteger (Name, 'Top',    Top);
      Width  := Ini.ReadInteger (Name, 'Width',  Width);
      Height := Ini.ReadInteger (Name, 'Height', Height);
    end;
  finally
    Ini.Free;
  end;
end;

procedure LoadChildControls(Parent: TWinControl);
var
  index, count: integer;
begin
  LoadControl(Parent);
  index := 0;
  count := Parent.ControlCount;
  while index < count do
  begin
    LoadControl(Parent.Controls[index]);
    LoadChildControls(TWinControl(Parent.Controls[index]));
    inc(index);
  end;
end;

procedure SaveChildControls(Parent: TWinControl);
var
  index, count: integer;
begin
  SaveControl(Parent);
  index := 0;
  count := Parent.ControlCount;
  while index < count do
  begin
    SaveControl(Parent.Controls[index]);
    SaveChildControls(TWinControl(Parent.Controls[index]));
    inc(index);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  LoadChildControls(self);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  SaveChildControls(self)
end;

procedure TForm1.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Panel3.Width := Round (Panel3.Width * NewWidth / Width);
  Panel4.Width := Round (Panel4.Width * NewWidth / Width);
end;

procedure TForm1.Panel1CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Panel2.Height := Round ( Panel2.Height * NewHeight / Panel1.Height);
  StringGrid1.Height := Round ( StringGrid1.Height * NewHeight / Panel1.Height);
end;

procedure TForm1.Panel2CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  ListView1.Width := Round (ListView1.Width * NewWidth / Panel2.Width );
  Panel5.Height := Round ( Panel5.Height * (Panel1.Height - NewHeight)/(Panel1.Height - Panel2.Height));
end;

procedure TForm1.Panel3CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Panel4.Width := Round ( Panel4.Width * (Width - NewWidth)/(Width - Panel3.Width));
end;

procedure TForm1.Panel4CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Panel3.Width := Round ( Panel3.Width * (Width - NewWidth)/(Width - Panel4.Width));
end;

procedure TForm1.Panel5CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Panel2.Height := Round ( Panel2.Height * (Panel1.Height - NewHeight)/(Panel1.Height - Panel5.Height));
end;

end.
